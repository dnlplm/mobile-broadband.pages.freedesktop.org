---
title: "Building libqrtr-glib 1.0 using GNU autotools"
linkTitle: "Autotools"
weight: 2
description: >
  How to build and install the libqrtr-glib library using GNU autotools.
---

**The last stable series with support for building with the GNU autotools suite is 1.0**. All the new stable series after 1.0 will exclusively use the [meson](/docs/libqmi/building/building-meson/) build system.

## Building from a release source tarball

The basic build and installation of the project can be done from an official release source tarball, in the following way:

```
  $ wget https://www.freedesktop.org/software/libqrtr-glib/libqrtr-glib-1.0.0.tar.xz
  $ tar -Jxvf libqrtr-glib-1.0.0.tar.xz
  $ cd libqrtr-glib-1.0.0
  $ ./configure --prefix=/usr
  $ make
```

## Optional switches

Additional optional switches that may be given to the `configure` command above would be:
 * In Debian/Ubuntu systems the default location for libraries depends on the architecture of the build, so instead of the default `/usr/lib` path that would be in effect due to `--prefix=/usr`, the user should also give an explicit `--libdir` path pointing to the correct location. E.g. on a 64bit Ubuntu/Debian build, the user would use `--prefix=/usr --libdir=/usr/lib/x86_64-linux-gnu`.
 * If the documentation should be rebuilt, the additional `--enable-gtk-doc` switch should be given. Omitting this switch will imply auto-detecting whether the documentation can be rebuilt with the already installed dependencies.
 * If the introspection support should be included in the build, the additional `--enable-introspection` switch should be given. Omitting this switch will imply auto-detecting whether the introspection can be built with the already installed dependencies.
 * When developing changes to the library or debugging issues, it is recommended to build with debug symbols so that running the program under `gdb` produces useful backtrace information. This can be achieved by providing user compiler flags like these: `CFLAGS="-ggdb -O0"`

An example project build using all the above optional switches could be:
```
  $ ./configure                          \
      --prefix=/usr                      \
      --libdir=/usr/lib/x86_64-linux-gnu \
      --enable-gtk-doc                   \
      --enable-introspection             \
      CFLAGS="-ggdb -O0"
  $ make
```

Running `./configure --help` will show all the possible switches that are supported.

## Building from a git checkout

When building from a git checkout, there is one single additional step required to build the project: running the included `autogen.sh` in order to setup the GNU autotools project and generate a `configure` script:

```
  $ git clone https://gitlab.freedesktop.org/mobile-broadband/libqrtr-glib.git
  $ cd libqrtr-glib
  $ NOCONFIGURE=1 ./autogen.sh
  $ ./configure --prefix=/usr
  $ make
```

The same optional switches may be given to the `configure` script when building from a git checkout.

## Installing

The installation on the prefix selected during `configure` can be done with the following command:
```
  $ sudo make install
```

Please note that the command above will install the library in the system default path for libraries, possibly overwriting any previous libqrtr-glib library that may already exist from a package manager installed package. See [the FAQ section](/docs/faq/#can-the-projects-be-installed-in-usrlocal-instead-of-usr) for comments on how to install in `/usr/local` instead.

## Uninstalling

If you have manually installed the project with the steps above, it can be uninstalled in the same way:

```
  $ sudo make uninstall
```

If the manual install overwrote the package manager installed files, it is suggested to force a re-install of the corresponding packages at this point, so that the system is not left with missing files.
