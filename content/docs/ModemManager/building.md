---
title: "Building"
linkTitle: "Building"
weight: 1
description: >
  How to build and install the ModemManager daemon and libraries.
---

The project currently uses the **GNU autotools** as build system.

There are two main ways to build the project: from a git repository checkout and from a source release tarball.

## Dependencies

Before you can compile the ModemManager project, you will need at least the following tools:
 * A compliant C toolchain: e.g. `glibc` or `musl libc`, `gcc` or `clang/llvm`, and `make`.
 * [pkg-config](https://www.freedesktop.org/wiki/Software/pkg-config/), a tool for tracking the compilation flags needed for libraries.
 * The [glib2](https://developer.gnome.org/glib/) library.
     * For ModemManager git master builds, glib2 >= 2.56.
     * For ModemManager >= 1.14, glib2 >= 2.48
     * For ModemManager >= 1.6 and < 1.14, glib2 >= 2.36
     * For ModemManager >= 1.0 and < 1.6, glib2 >= 2.32
 * The [libgudev](https://wiki.gnome.org/Projects/libgudev) library.
     * For ModemManager git master builds, libgudev >= 232.
     * For ModemManager >= 1.0 and < 1.18, libgudev >= 147.
 * The [gettext](https://www.gnu.org/software/gettext) tools.
     * For ModemManager >= 1.8, gettext >= 0.19.8.
     * For ModemManager >= 1.6 and < 1.8, gettext >= 0.19.3.
     * For ModemManager >= 1.0 and < 1.6, gettext >= 0.17.

If building old releases, this is an additional requirement that no longer applies in newer releases:
 * The [intltool](https://freedesktop.org/wiki/Software/intltool) tools:
     * For ModemManager >= 1.0 and < 1.8, intltool >= 0.40.

In addition to the previous mandatory requirements, the project also has several optional dependencies that would be needed when enabling additional project features:
 * **libmbim**, in order to use the MBIM protocol.
     * For ModemManager git master builds, libmbim git master.
     * For ModemManager >= 1.14, libmbim >= 1.24.
     * For ModemManager >= 1.8 and < 1.14, libmbim >= 1.18.
     * For ModemManager >= 1.6 and < 1.8, libmbim >= 1.14.
     * For ModemManager >= 1.0 and < 1.6, libmbim >= 1.4.
 * **libqmi**, in order to use the QMI protocol.
     * For ModemManager git master builds, libqmi git master.
     * For ModemManager >= 1.16, libqmi >= 1.28.
     * For ModemManager >= 1.14 and < 1.16, libqmi >= 1.26.
     * For ModemManager >= 1.12 and < 1.14, libqmi >= 1.24.
     * For ModemManager >= 1.10 and < 1.12, libqmi >= 1.22.
     * For ModemManager >= 1.8 and < 1.10, libqmi >= 1.20.
     * For ModemManager >= 1.6 and < 1.8, libqmi >= 1.16.
     * For ModemManager >= 1.2 and < 1.6, libqmi >= 1.6.
     * For ModemManager >= 1.0 and < 1.2, libqmi >= 1.4.
 * **libqrtr-glib**, in order to use Qualcomm SoCs with the QRTR bus.
     * For ModemManager git master builds, libqrtr-glib >= 1.0.
 * [libpolkit-gobject](https://freedesktop.org/wiki/Software/polkit) >= 0.97, in order to allow access control on the DBus methods.
 * [systemd](https://www.freedesktop.org/wiki/Software/systemd) support libraries (libsystemd >= 209 or libsystemd-login >= 183), for the optional suspend and resume support.
 * [gtk-doc](https://developer.gnome.org/gtk-doc-manual/stable) tools, in order to regenerate the documentation.
 * [gobject-introspection](https://gi.readthedocs.io), in order to generate the introspection support.

When building from a git checkout instead of from a source tarball, the following additional dependencies are required:
 * GNU autotools ([autoconf](https://www.gnu.org/software/autoconf/)/[automake](https://www.gnu.org/software/automake)/[libtool](https://www.gnu.org/software/libtool/)).
 * [Autoconf archive](https://www.gnu.org/software/autoconf-archive)

## Building

The basic build and installation of the project can be done from an official release source tarball, in the following way:

```
  $ wget https://www.freedesktop.org/software/ModemManager/ModemManager-1.16.4.tar.xz
  $ tar -Jxvf ModemManager-1.16.4.tar.xz
  $ cd ModemManager-1.16.4
  $ ./configure --prefix=/usr
  $ make
```

### Optional switches

Additional optional switches that may be given to the `configure` command above would be:
 * In Debian/Ubuntu systems the default location for libraries depends on the architecture of the build, so instead of the default `/usr/lib` path that would be in effect due to `--prefix=/usr`, the user should also give an explicit `--libdir` path pointing to the correct location. E.g. on a 64bit Ubuntu/Debian build, the user would use `--prefix=/usr --libdir=/usr/lib/x86_64-linux-gnu`.
 * If the MBIM protocol support is required, the additional `--with-mbim` should be given. Omitting this switch will imply auto-detecting whether the feature can be built with the already installed dependencies.
 * If the QMI protocol support is required, the additional `--with-qmi` should be given. Omitting this switch will imply auto-detecting whether the feature can be built with the already installed dependencies.
 * If the suspend/resume handling is required, the additional `--with-suspend-resume=systemd` should be given. Omitting this switch will imply auto-detecting whether the feature can be built with the already installed dependencies.
 * If the documentation should be rebuilt, the additional `--enable-gtk-doc` switch should be given. Omitting this switch will imply auto-detecting whether the documentation can be rebuilt with the already installed dependencies.
 * If the introspection support for the `libmm-glib` library should be included in the build, the additional `--enable-introspection` switch should be given. Omitting this switch will imply auto-detecting whether the introspection can be built with the already installed dependencies.
 * When developing changes to the project or debugging issues, it is recommended to build with debug symbols so that running the program under `gdb` produces useful backtrace information. This can be achieved by providing user compiler flags like these: `CFLAGS="-ggdb -O0"`

An example project build using all the above optional switches could be:
```
  $ ./configure                          \
      --prefix=/usr                      \
      --libdir=/usr/lib/x86_64-linux-gnu \
      --with-qmi                         \
      --with-mbim                        \
      --with-suspend-resume=systemd      \
      --enable-gtk-doc                   \
      --enable-introspection             \
      CFLAGS="-ggdb -O0"
  $ make
```

Running `./configure --help` will show all the possible switches that are supported.

### Building from a git checkout

When building from a git checkout, there is one single additional step required to build the project: running the included `autogen.sh` in order to setup the GNU autotools project and generate a `configure` script:

```
  $ git clone https://gitlab.freedesktop.org/mobile-broadband/ModemManager.git
  $ cd ModemManager
  $ NOCONFIGURE=1 ./autogen.sh
  $ ./configure --prefix=/usr
  $ make
```

The same optional switches may be given to the `configure` script when building from a git checkout.

## Installing

The installation on the prefix selected during `configure` can be done with the following command:
```
  $ sudo make install
```

Please note that the command above will install the library in the system default path for libraries, possibly overwriting any previous libqmi library that may already exist from a package manager installed package.

### Installing in /usr/local

Installing in a prefix different to the suggested `/usr` one, like the default `/usr/local`, has multiple issues in the ModemManager project. These are the reasons why this guide suggests to overwrite the package manager installed files.

#### Location of the libmm-glib library

If no explicit `--prefix` is given to the `configure` command when preparing the build, the default `/usr/local` path will be used as prefix, which will avoid needing to overwrite the package manager installed files. If this approach is taken, the developer can keep the package manager installed library in `/usr/lib`, the package manager installed command line tool in `/usr/bin` and the package manager installed daemon in `/usr/sbin`, while keeping the custom installed library in `/usr/local/lib`, the custom installed command line tool in `/usr/local/bin` and the custom installed daemon in `/usr/local/sbin`.

Under this scenario, the user can run the package manager installed `ModemManager` daemon in the following way, which will use the package manager installed `libmm-glib` library from `/usr/lib`:
```
  $ sudo /usr/sbin/ModemManager
```

In the same scenario, the user can run the custom installed `ModemManager` daemon in the following way, which will use the custom installed `libmm-glib` library from `/usr/local/lib`:
```
  $ sudo LD_LIBRARY_PATH=/usr/local/lib /usr/local/sbin/ModemManager
```

But **beware** because there are distributions like Debian/Ubuntu which add the `/usr/local/bin` path to the `PATH` environment variable by default, so running the following command would attempt to run the custom installed `ModemManager` daemon with the package manager installed `libmm-glib` library from `/usr/lib` (which is obviously a mess and won't probably work):
```
  $ sudo ModemManager
```

#### Location of the DBus service files

By default the DBus service files provided by ModemManager are installed under `<prefix>/share/dbus-1/system.d/`. If the user uses the default `/usr/local` prefix instead of the suggested `/usr` prefix, the DBus service files would be installed in a path that is completely ignored by DBus. If the service files haven't changed with respect to the one provided by the package manager, this issue can probably be ignored.

#### Location of the udev rules

By default the udev rules provided by ModemManager are installed under `/lib/udev/rules.d/`, out of the prefix provided by the user. If the user uses the default `/usr/local` prefix instead of the suggested `/usr` prefix, the udev rules would not change location. Changing the location of where the udev rules are installed so that the package manager provided ones are not overwritten can be done e.g. with `--with-udev-base-dir=/usr/local/lib/udev/rules.d`, but note that these rules will not be actively used by udev as they are not installed in a standard path.

#### Location of the systemd init files

By default the systemd init files provided by ModemManager are installed under the path specified by the systemd library `pkg-config` file in the `systemdsystemunitdir` variable, e.g. `/lib/systemd/system/`. This path is completely independent to the prefix provided by the user. Changing the location of where the systemd init files are installed so that the package manager provided ones are not overwritten can be done e.g. with `--with-systemdsystemunitdir=/usr/local/lib/systemd/system`, but note that the init file will not be actively used by systemd as it is not installed in a standard path.

## Uninstalling

If you have manually installed the project with the steps above, it can be uninstalled in the same way:

```
  $ sudo make uninstall
```

If the manual install overwrote the package manager installed files, it is suggested to force a re-install of the corresponding packages at this point, so that the system is not left with missing files.

## API/ABI

The `libmm-glib` library and DBus API are always API/ABI compatible, so installing a newer version (e.g. self compiled) of the project on a system that has other packages depending on an older version will not break the setup.

Installing an older version of the project on a system that has other packages depending on a newer version may obviously break the setup, though.