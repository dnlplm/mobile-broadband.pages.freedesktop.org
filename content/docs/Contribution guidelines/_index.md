---
title: "Contribution Guidelines"
linkTitle: "Contribution Guidelines"
weight: 10
description: >
  How to contribute to the projects
---

This section provides information about how users can contribute to the project, either by reporting **issues**, suggesting **new features** or developing **fixes**.
