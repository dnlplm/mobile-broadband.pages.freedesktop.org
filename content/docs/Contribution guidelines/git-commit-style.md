---
title: "git commit style"
linkTitle: "git commit style"
weight: 3
description: >
  How the git commits should look like
---

Having a proper style in the git commits suggested for inclusion in the projects is also a very important thing. This section provides some hints of what is expected from the git commits suggested by contributors.

As with the source code coding style, the best way to see what kind of git commit format is expected is to look at other commits from other developers in the same project.

## Authorship

The author of the git commit should preferably a real person name (Firstname Lastname), as the author of the email is implicitly the one who holds the copyright of the changes done in the commit, unless explicitly stated e.g. with additional copyright lines in the source code.

## Basic message formatting

The message of a git commit must be composed of:
- An initial first line with a short description of the change, prefixed by a "module: " string. The *module* could be some specific object in the code, or some protocol, or some other similar thing. If unsure, the best thing to get a feeling of what you should use as *module* would be to run `git log <FILE>` (being <FILE> the main file being modified in the commit) and read what other developers have used as *module* when modifying that same file.
- Then an empty whiteline.
- Then the body of the commit, including an optional longer description. This would be optional only for trivial changes for which the first description line is enough. For any change that is a bit more complex, the longer description can be considered mandatory. The body of the message may be composed by multiple paragraphs, and may also include additional information like real program logs, crash backtraces reported by gdb,  or even valgrind memcheck log snippets (e.g. when fixing memory leaks).

For text content in the commit message (either first line or body) it is suggested to keep the maximum line length at 70 characteres maximum. This is not a strict rule for the first line though, sometimes just a few more characters are needed, and that would be acceptable.

For log snippets or backtraces included in the commit message, it is suggested to indent them a few characters. Also, there is no line length limitation in this case, but it is suggested to pretty-format them so that the information included is all valuable. E.g. when inserting program logs in the commit message, the developer may first clean them up to remove unrelated log lines, or even remove information that isn't necessary (e.g. maybe the timestamps are irrelevant for the purpose of the included information, so they can be removed).

Commits may have a `Signed-off-by:` line (e.g. as per `git commit -s`) but this is not strictly necessary.

When fixing issues reported in the project gitlab, a `Fixes <Bug URL>` line is suggested. When this commit is merged to the project git master, gitlab will automatically close the referred issue.

### Examples

This is a properly formatted commit message:
```
base-modem: plug leaks when organizing ports

The GLists maintained in the logic need to be explicitly freed (just
the lists, not the list contents) if we exit early on error or if we
end up deciding that the specific ports are available but unsupported
by the plugin (e.g. if a plugin that doesn't support net ports finds
net ports in the modem).

  ==225333== 24 bytes in 1 blocks are definitely lost in loss record 2,024 of 5,525
  ==225333==    at 0x483E77F: malloc (vg_replace_malloc.c:307)
  ==225333==    by 0x506C539: g_malloc (in /usr/lib/libglib-2.0.so.0.6600.7)
  ==225333==    by 0x508DC8F: g_slice_alloc (in /usr/lib/libglib-2.0.so.0.6600.7)
  ==225333==    by 0x50636B4: g_list_append (in /usr/lib/libglib-2.0.so.0.6600.7)
  ==225333==    by 0x17E671: mm_base_modem_organize_ports (mm-base-modem.c:1298)
  ==225333==    by 0x1E4409: mm_plugin_create_modem (mm-plugin.c:1094)
  ==225333==    by 0x162C81: mm_device_create_modem (mm-device.c:481)
  ==225333==    by 0x15DE60: device_support_check_ready (mm-base-manager.c:218)
  ==225333==    by 0x4EA8173: ??? (in /usr/lib/libgio-2.0.so.0.6600.7)
  ==225333==    by 0x4EAC6E8: ??? (in /usr/lib/libgio-2.0.so.0.6600.7)
  ==225333==    by 0x16730F: device_context_run_ready (mm-plugin-manager.c:1533)
  ==225333==    by 0x4EA8173: ??? (in /usr/lib/libgio-2.0.so.0.6600.7)
```

These are **NOT** a properly formatted commit messages:

```
Update src/mm-broadband-modem.c, update src/mm-broadband-modem.h, update src/mm-modem-helpers.c, update src/mm-modem-helpers.h, update src/mm-base-modem.h .......
```
(lacks description of what the commit is doing)

```
Added dual sim support to Quectel plugin
```
(no `module: ` prefix; e.g. should have been `quectel: added dual sim support`)

```
mm-broadband-modem: Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut turpis turpis, euismod at gravida at, tincidunt at metus. Maecenas placerat laoreet arcu non luctus. Duis sit amet augue ullamcorper, tincidunt magna ac, porttitor metus.
```
(first line more than 70 chars)


## Contents of the commit

The maintainers of the project will review the commits one by one and therefore, each commit should be self-contained:
- A single commit must only provide one single logical change. It is acceptable to include in the commit additional minor coding style modifications affecting the code being fixed, but that's it.
- If the changes being suggested in a merge request are a lot, it is therefore expected to have a series of commits implementing the changes. Merge requests that contain one single commit with 5000 line changes across the project will be rejected right away.
- Commits fixing only coding style are also accepted. This is probably an exception to most free/open source software projects, so don't assume this to be the default elsewhere.

The changes done in a single commit must **NEVER** break the build, because otherwise git bisect-ing would not be possible.
